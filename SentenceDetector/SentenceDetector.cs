﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypeTwo
{
    class StringSplitter
    {
        /**
         * Splits a string into a list of strings where each
         * string in the list is a sentence from the input string
         * The resulting list has sentences in the original order.
         */
        public static List<String> detectSentences(String input)
        {
            List<String> output = new List<String>();
            StringBuilder sentence = new StringBuilder();
            foreach(char c in input.ToCharArray()) {
                sentence.Append(c);
                switch(c) {
                    case '.':
                    case '?':
                    case '!':
                        output.Add(sentence.ToString().Trim());
                        sentence = new StringBuilder();
                        break;
                    default:
                        break;

                }
            }
            return output;
        }

        /**
         * Splits an input string into a list of strings, representing the individual
         * words in the input sentence
         */
        public static List<String> splitIntoWords(String input)
        {
            List<String> output = new List<String>();
            StringBuilder word = new StringBuilder();
            foreach (char c in input.ToCharArray())
            {
                word.Append(c);
                switch (c)
                {
                    case ' ':
                        output.Add(word.ToString().Trim());
                        word = new StringBuilder();
                        break;
                    default:
                        break;

                }
            }
            if (word.ToString().Trim().Length > 0) output.Add(word.ToString().Trim());
            return output;
        }
    }
}
