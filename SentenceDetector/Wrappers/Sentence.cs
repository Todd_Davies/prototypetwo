﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SentenceDetectorBlock.Wrappers
{
    class Sentence
    {
        private String contents;
        private List<Word> wordContents;

        public Sentence(String c)
        {
            contents = c;
        }

        public void setWords(List<Word> words)
        {
            wordContents = words;
            contents = null;
        }

        public List<Word> getWords() { return wordContents; }

        public String getContents()
        {
            if (contents == null && wordContents != null) return string.Join(" ", wordContents);
            else return contents;
        }
    }
}
