﻿using PrototypeTwo;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace SentenceDetectorBlock
{
    /**
     * Splits a string into n equal blocks
     */
    class StringSplitterBlock : AbstractTransformManyBlock<String, String>
    {
        public StringSplitterBlock(int numSections)
            : base(new TransformManyBlock<String, String>(input => {
                int length = input.Length;
                int sectionLength = length / numSections;
                ConcurrentQueue<String> sections = new ConcurrentQueue<String>();
                if (length <= numSections)
                {
                    sections.Enqueue(input);
                }
                else
                {
                    int index = 0;
                    while (index < length)
                    {
                        if (index + sectionLength >= length)
                        {
                            sections.Enqueue(input.Substring(index));
                        }
                        else
                        {
                            sections.Enqueue(input.Substring(index, sectionLength));
                        }
                        index += sectionLength;
                    }
                }
                return sections;
        }))
        { }
    }
}
