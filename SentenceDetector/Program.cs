﻿using SentenceDetectorBlock.Wrappers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace PrototypeTwo
{
    class Program
    {
        static void Main(String[] args)
        {
            // The input string we're going to work with
            String input = "Hi there, I'm Todd! What's your name? Is it Bob?";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 1; i++) sb.Append(input);
            input = sb.ToString(); ;
            // Serial:
            Console.WriteLine("Serial version:");
            Stopwatch stopwatch = Stopwatch.StartNew();
            List<String> sentences = StringSplitter.detectSentences(input);
            List<List<String>> words = new List<List<String>>();
            // First split into sentences
            foreach(String sentence in sentences) {
                words.Add(StringSplitter.splitIntoWords(sentence));
            }
            // Then split each into words
            foreach(List<String> sentence in words)
            {
                foreach (String word in sentence) { }// Console.WriteLine(word);
            }
            stopwatch.Stop();
            Console.WriteLine("Split in {0:N}ms:", stopwatch.ElapsedMilliseconds);
            words = null;
            sentences = null;

            Console.ReadLine();
            Console.WriteLine("----");

            // Parallel:
            Console.WriteLine("Parallel version:");
            // Create the blocks needed to split into sentences, then words (and the buffer blocks)
            BufferBlock<String> inputBuffer = new BufferBlock<String>();
            SentenceDetectorBlock sentenceDetector = new SentenceDetectorBlock();
            BufferBlock<String> wordBuffer = new BufferBlock<String>();
            WordSplitterBlock wordSplitter = new WordSplitterBlock();
            ActionBlock<String> consoleOut = new ActionBlock<String>(_ => { 
                Console.WriteLine(_);
            });
            // Link the blocks together and define their completion logic
            inputBuffer.LinkTo(sentenceDetector);
            inputBuffer.Completion.ContinueWith(delegate { sentenceDetector.Complete(); });
            sentenceDetector.LinkTo(wordBuffer);
            sentenceDetector.Completion.ContinueWith(delegate {
                Console.WriteLine("Finished detecting sentences", 0);
                wordBuffer.Complete();
            });
            wordBuffer.LinkTo(wordSplitter);
            sentenceDetector.Completion.ContinueWith(delegate
            {
                wordSplitter.Complete();
            });
            wordSplitter.LinkTo(consoleOut);
            wordSplitter.Completion.ContinueWith(delegate {
                Console.WriteLine("Finished word splitting", 0);
                consoleOut.Complete();
            });
            consoleOut.Completion.ContinueWith((arg) => {
                stopwatch.Stop();
                Console.WriteLine("Took {0:N}ms:", stopwatch.ElapsedMilliseconds);
            });
            // Start the stopwatch and push the data into the dataflow
            stopwatch = Stopwatch.StartNew();
            inputBuffer.Post(input);
            inputBuffer.Complete();
            Console.ReadLine();
        }
    }
}
