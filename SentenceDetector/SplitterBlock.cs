﻿using PrototypeTwo;
using SentenceDetectorBlock.Wrappers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace PrototypeTwo
{
    /**
     * Provides a block for splitting an input String into a list of
     * Strings based on supplied delimiters.
     */
    class SplitterBlock : AbstractTransformManyBlock<String, String>
    {
        public SplitterBlock(params char[] delimeters)
            : base(new TransformManyBlock<String, String>(input => {
                var words = new ConcurrentQueue<String>();
                int lastIndex = 0, currentIndex = 0;
                foreach (char c in input.ToCharArray())
                {
                    currentIndex++;
                    if (delimeters.Contains(c))
                    {
                        words.Enqueue(input.Substring(lastIndex, currentIndex - lastIndex));
                        lastIndex = currentIndex;
                    }
                }
                if (lastIndex < currentIndex) words.Enqueue(input.Substring(lastIndex));
                return words;
        }, new ExecutionDataflowBlockOptions
         {
            MaxDegreeOfParallelism = Environment.ProcessorCount
         }))
        { }
    }
}
