﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace PrototypeTwo
{
    /**
     * Splits the input into  words
     */
    class WordSplitterBlock : SplitterBlock
    {
        public WordSplitterBlock() : base(' ') {}
    }
}
